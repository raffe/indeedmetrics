function dragin(e) { //function for drag into element, just turns the bix X white
  $(dropzone).addClass('hover')
}

function dragout(e) { //function for dragging out of element
  $(dropzone).removeClass('hover')
}

function drop(e) {
  var file = this.files[0]
  $('#dropzone').removeClass('hover').addClass('dropped')
  $('#file-browse-trigger').addClass('is-loading')
  // console.log("file", file)

  var data = new FormData()
  data.append('user[active_csv]', file)
  // data.append('user', 'hubot')
  fetch('/users/1.json', _.merge({
    method: "PATCH",
    credentials: 'include',
    headers: {
      // "Accept": "application/json",
      // "Content-Type": "application/json",
      "X-CSRF-Token": document.querySelector('meta[name="csrf-token"]').getAttribute("content"),
      // "X-Requested-With" : "XMLHttpRequest"
    }
  }, { body: data }))
  .then(function(response){
    location.reload()
  })

  // fetch('/users/1', {
  //   method: 'PUT',
  //   body: data
  // })
}

$(document).on('ready', function(e){
  var dropzone = $("#dropzone")
  var input    = dropzone.find('input')

  dropzone.on({
      dragenter : dragin,
      dragleave : dragout
  })
  input.on('change', drop)
})


$(document).on('click', '#file-browse-trigger', function(e){
  $('input[type="file"]').click()
})

$(document).on('ready', function(e){
  $('#captures-table').DataTable()
  // $('#users-table').DataTable()
})

$(document).on('click', '.js-form-show-trigger', function(e) {
  e.preventDefault()
  var triggers = $(this).attr('data-triggers')
  $('#'+triggers).removeClass('hide')
  $(this).removeClass('js-form-show-trigger').addClass('js-form-hide-trigger').children('.fa').removeClass('fa-cogs').addClass('fa-times-circle-o')
  // console.log($(this).children('.fa'))
})

$(document).on('click', '.js-form-hide-trigger', function(e) {
  e.preventDefault()
  var triggers = $(this).attr('data-triggers')
  $('#'+triggers).addClass('hide')
  $(this).addClass('js-form-show-trigger').removeClass('js-form-hide-trigger').children('.fa').addClass('fa-cogs').removeClass('fa-times-circle-o')
  // console.log($(this).children('.fa'))
})

$(document).on('click', '.js-form-hide-trigger-form', function(e) {
  e.preventDefault()
  var triggers = $(this).attr('data-triggers')
  var trigger = $(this).attr('data-id')
  $('#'+triggers).addClass('hide')
  $('#'+trigger).addClass('js-form-show-trigger').removeClass('js-form-hide-trigger').children('.fa').addClass('fa-cogs').removeClass('fa-times-circle-o')
  // console.log($(this).children('.fa'))
})

$(document).on('click', '.js-trigger-email-export', function(e){
  e.preventDefault()
});

$(document).on('click', '.notification > button.delete', function() {
  $(this).parent().addClass('is-hidden').remove()
});

$(document).on('ready', function() {
  if($('.server-side.notification').length > 0){
    setTimeout(function(e){
      $('.server-side.notification').fadeOut( "slow" )
    }, 3500)
  }
});

function checkPasswordMatch() {
  var password = $('input[name="user[password]"]').val();
  var confirmPassword = $('input[name="user[password_confirmation]"]').val();
  if (password != confirmPassword){
    $("#password-checker").removeClass('is-hidden').addClass('is-danger').html("New password & password confirmation do not match")
    $('input[name="user[password]"]').addClass('is-danger')
    $('input[name="user[password_confirmation]"]').addClass('is-danger')
    $('#form-submit').prop('disabled', true)
  }
  else{
    $("#password-checker").addClass('is-hidden').removeClass('is-danger').html()
    $('input[name="user[password]"]').removeClass('is-danger')
    $('input[name="user[password_confirmation]"]').removeClass('is-danger')
    $('#form-submit').prop('disabled', false)
  }
}

$(document).ready(function () {
   $('input[name="user[password]"], input[name="user[password_confirmation]"]').keyup(checkPasswordMatch);
});

$(document).on('click', 'a[href*="#"]:not([href="#"])', function(e) {
  $('a.item.active').removeClass('active')
  $(this).addClass('active')
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  }
});
