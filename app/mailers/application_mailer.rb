class ApplicationMailer < ActionMailer::Base
  default from: 'raffe@weareinhouse.com'
  layout 'mailer'
end
