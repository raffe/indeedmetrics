class UsersController < ApplicationController
  def update
    @user = User.find(params[:id])
    if !params[:user][:current_password].blank?
      unless @user.valid_password?(params[:user][:current_password])
        flash[:error] = "Current Password Incorrect"
        redirect_back(fallback_location: dashboard_path) and return
      end
    end

    if @user.custom_update_attributes(user_params)
      flash[:success] = "Update Successful"
      respond_to do |format|
        format.html { redirect_back(fallback_location: dashboard_path) and return }
        format.js {}
        format.json { render json: {user: @user} }
      end
    else
      byebug
      flash[:error] = "Edit Unsuccessful. Errors: #{@user.errors.full_messages.each{|error| puts error}}"
      respond_to do |format|
        format.html { redirect_back(fallback_location: dashboard_path) and return }
        format.js {}
        format.json { render json: {user: @user, errors: @user.errors} }
      end
    end
  end

  private
    def user_params
      params.require(:user).permit(:active_csv, :email, :password, :password_confirmation)
    end
end
