import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Company from '@/components/Company'
import Module from '@/components/Module' 
import PdfExport from '@/components/PdfExport' 
import Success from '@/components/Success' 
import PdfPrint from '@/components/PdfPrint' 

Vue.use(Router)

export default new Router({
  	mode: 'history',
  	routes: [
	    {
          path: '/',
          name: 'home',
          component: Home
	    },
    	{
      		path: '/clear',
      		name: 'clear'
    	},
    	{
      		path: '/company/:id',
      		name: 'company',
      		component: Company
    	},
      {
          path: '/company/:id/export',
          name: 'pdf-export',
          component: PdfExport
      },
      {
          path: '/company/:id/print/:modules',
          name: 'pdf-print',
          component: PdfPrint
      },
      {
          path: '/company/:id/:moduleType',
          name: 'module',
          component: Module
      },
      {
          path: '/success',
          name: 'success',
          component: Success
      }
  	]
})
