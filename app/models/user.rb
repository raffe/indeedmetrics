class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_attached_file :active_csv
  do_not_validate_attachment_file_type :active_csv
  # validates_attachment_content_type :active_csv, content_type: /\Aimage\/.*\z/
  validates_attachment_content_type :active_csv, :content_type => %w(application/vnd.ms-office application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet application/vnd.ms-excel application/octet-stream text/csv text/plain text/comma-separated-values)

  def custom_update_attributes(params)
    if params[:password].blank?
      params.delete :password
      params.delete :password_confirmation
      update_attributes params
    end
  end
end
