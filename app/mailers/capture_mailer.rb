class CaptureMailer < ApplicationMailer
  def send_email(options={})
    @first_name = options[:first_name]
    @email = options[:email]
    @last_name = options[:last_name]
    @company_name = options[:company_name]
    @phone_number = options[:phone_number]
    @pdf = options[:pdf]
    mail(to: options[:email], subject: "Example Subject")
  end

  def send_export_email(options={})
    @email = options[:email]
    @csv = options[:csv]
    mail(to: options[:email], subject: "CSV export of Email Captures")
  end
end
