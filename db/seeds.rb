# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: "example@example.com", password: "s3cur33x4mp1310g1n", password_confirmation: "s3cur33x4mp1310g1n", role: "admin")
User.create(email: "regular@example.com", password: "s3cur33x4mp1310g1n", password_confirmation: "s3cur33x4mp1310g1n", role: "regular")
Capture.create(email: "capture@example.com", first_name: "Marco", last_name: "Polo", company_name: "Mike & Ike", phone_number: "1234511223")
