// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import { sync } from 'vuex-router-sync'
import Parser from 'papaparse'
import { ready } from './Utils.js'

Vue.use(Vuex)
Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    entries: [],
    isEntriesLoaded: false,
    csvUrl: '',
    term: '',
    chartTypes: {
      'performance': 'Resumes / Performance for Top Jobs',
      'overall': 'Resumes / Overall Performance',
      'response-rate': 'Response Rate',
      'experience-education': 'Resumes / Experience / Education'
    },
    fontFamily: 'Helvetica Neue, Helvetica, Verdana, Segoe UI, sans-serif'
  },
  mutations: {
    ADD_ENTRIES (state, payload) {
      state.entries = state.entries.concat(payload.entries)
      state.isEntriesLoaded = true
    },
    CLEAR_ENTRIES (state) {
      state.entries = []
      state.isEntriesLoaded = false
    },
    SEARCH_TERM (state, payload) {
      state.term = payload.term
    }
  }
})

sync(store, router)

router.beforeEach((to, from, next) => {
    if (to.path == '/clear') {
      store.commit('CLEAR_ENTRIES')
      return next('/')
    }
    if (!store.state.isEntriesLoaded) {
      Parser.parse(document.getElementById('active-csv').getAttribute('data-csv'), {  
        download: true,
        header: true,
        complete (data) {
          let incremented = data.data.map((d, i) => {
            d.Source = d.Source
            return d
          })
          store.commit('ADD_ENTRIES', {
            entries: incremented
          })
          next()
        }
      })
    } else {
      next()
    }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  mounted () {
    ready()
  }
})
