Rails.application.routes.draw do
  devise_for :users
  root "pages#app"
  get "company/:id" => "pages#app"
  get "company/:id/:moduleType" => "pages#app"
  get "company/:id/export" => "pages#app"
  get "company/:id/print/:modules" => "pages#app"
  get "clear" => "pages#app"
  get "success" => "pages#app"
  get 'dashboard', to: 'pages#dashboard', as: 'dashboard'
  resources :users, only: [:update]
  resources :captures, only: [:create, :index]
  post "/dashboard/send-email", to: 'pages#send_export_email', as: 'send_export_email'
end
