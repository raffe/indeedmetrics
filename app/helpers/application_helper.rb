module ApplicationHelper
  def bulma_class_for flash_type
    { success: "is-success", error: "is-danger", alert: "is-danger", notice: "is-success" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "server-side notification #{bulma_class_for(msg_type)}") do
              concat content_tag(:button, '', class: "delete", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end
end
