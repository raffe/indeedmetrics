export function triggerMap (el) {
	let style = el.currentStyle || window.getComputedStyle(el, false)
	let imageUrl = style.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2').split(',')[0];

	el.style.backgroundImage = 'none'
	el.style.backgroundImage = 'url(' + imageUrl + '?' + Date.now() + ')'
}

export function ready () {


}