class AddAttachmentActiveCsvToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :active_csv
    end
  end

  def self.down
    remove_attachment :users, :active_csv
  end
end
