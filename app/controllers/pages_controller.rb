class PagesController < ApplicationController
  def app
    @active_csv = User.find(1).active_csv
  end

  def dashboard
    if current_user.role != "admin"
      redirect_back(fallback_location: root_path)
    end
  end

  def send_export_email
    CaptureMailer.send_export_email({email: "raffe90@gmail.com", csv: User.find(1).active_csv.url}).deliver
    flash[:success] = "Email sent! You will receive an email shortly."
    redirect_back(fallback_location: dashboard_path)
  end
end
