class Capture < ApplicationRecord
  has_attached_file :pdf
  validates_attachment_content_type :pdf, :content_type => %w(application/pdf)

  def self.to_csv
    attributes = %w{id first_name last_name email phone_number company_name}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |email_capture|
        csv << attributes.map{ |attr| email_capture.send(attr) }
      end
    end
  end

  def as_json(options={})
    super(options.merge({:methods => [:pdf]}))
  end
end
