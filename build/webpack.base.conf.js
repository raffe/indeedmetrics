var path = require('path')
var utils = require('./utils')
var config = require('../config')
var vueLoaderConfig = require('./vue-loader.conf')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  entry: {
    app: './src/main.js'
  },
  // output: {
  //   path: config.build.assetsRoot,
  //   filename: '[name].js',
  //   publicPath: process.env.NODE_ENV === 'production'
  //     ? config.build.assetsPublicPath
  //     : config.dev.assetsPublicPath
  // },
  target: 'web', // necessary per https://webpack.github.io/docs/testing.html#compile-and-test
  output: {
    filename: 'client-bundle.js',
    path: './app/assets/javascripts/'
  },
  resolve: {
    extensions: ['.js', '.vue', '.json', '.eot', '.woff', '.woff2', '.svg'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src'),
      'images': resolve('src/assets/images'),
      'styles': resolve('src/assets/styles'),
      'fonts': resolve('src/assets/fonts'),
      'static': resolve('src/assets/static')
    }
  },
  module: {
    rules: [
      /*{
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: "pre",
        include: [resolve('src'), resolve('test')],
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      },*/
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
    {
      test: /\.sass$/,
      use: ExtractTextPlugin.extract({
        // fallback: 'style-loader',
        use: ['style!css!resolve-url!sass?indentedSyntax=true&sourceMap=true']
      })
    },
      // {
      //   test: /\.(sass)$/,
      //   loader: ExtractTextPlugin.extract({use:'style!css!resolve-url!sass?indentedSyntax=true&sourceMap=true'}),
      // },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('src'), resolve('test')]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        query: {
          limit: 10000,
          name: utils.assetsPath('images/[name].[ext]')
        }
      },
      {
        test: /\.csv$/,
        loader: 'file-loader',
        query: {
          name: utils.assetsPath('static/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf)(\?.*$|$)/,
        loader: 'url-loader?importLoaders=1&limit=100000&name=' + utils.assetsPath('fonts/[name].[hash:7].[ext]')
      }
    ]
  }
}
