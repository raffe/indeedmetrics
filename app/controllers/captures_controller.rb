class CapturesController < ApplicationController
  def index
    @captures = Capture.all

    respond_to do |format|
      format.csv { send_data @captures.to_csv, filename: "email-captures-#{Date.today}.csv" }
    end
  end

  def create
    @capture = Capture.new(capture_params)
    @html = params[:capture][:pdf]
    @filename = "#{@capture.first_name}_#{@capture.last_name}_#{@capture.company_name}_#{Date.today}"
    pdf = render_to_string pdf: @filename, layout: 'pdf.html.erb', zoom: 1.0, template: "captures/render.pdf.erb", page_size: 'letter', encoding: "utf-8", javascript_delay: 50, orientation: 'portrait', lowquality: false, no_pdf_compression: false, margin:  { top:0, bottom: 0, left: 0, right: 0 }

    Tempfile.open(["#{@filename}" , ".pdf"] , Rails.root.join('tmp')) do |f|
      f << pdf.force_encoding("UTF-8")
      @capture.pdf = f
      @capture.save
    end

    if @capture.valid?
      # puts @capture.pdf.url
      CaptureMailer.send_email({
        first_name: @capture.first_name,
        last_name: @capture.last_name,
        company_name: @capture.company_name,
        phone_number: @capture.phone_number,
        email: @capture.email,
        pdf: @capture.pdf.url}).deliver
      render json: { capture: @capture, :message => "Capture created.", ok: true, status: 200}, :status => :ok
    else
      render json: { capture: @capture, errors: @capture.errors, :message => "Unable to create capture.", ok: false, status: 422}, :status => :unprocessable_entity
    end
  end

  private
    def capture_params
      params.require(:capture).permit(:first_name, :last_name, :email, :phone_number, :company_name)
    end
end
